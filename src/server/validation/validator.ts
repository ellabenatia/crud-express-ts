import { Request ,Response, NextFunction } from "express";
import Joi from "joi";

const schema = Joi.object({
    first_name: Joi.string()
        .pattern(new RegExp(/[a-zA-Z]+/))
        .min(3)
        .max(30)
        .required(),
    last_name: Joi.string()
    .pattern(new RegExp(/[a-zA-Z]+/))
    .min(3)
    .max(30)
    .required(),
    email: Joi.string().email(),
    phone : Joi.string().pattern(/[0-9]{8,12}/).required()
})
export async function checkUser(req:Request,res:Response,next:NextFunction){
    try{
       await schema.validateAsync(req.body);
        next();
    }
    catch(err){
        console.log("catch");
        next(err);
    }
}


//console.log(schema.validate({ first_name: 'abcdd', last_name: "ekkaa", email:"lel@gmail.com", phone:"55455445454" }));
