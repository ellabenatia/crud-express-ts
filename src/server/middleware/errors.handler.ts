/* eslint-disable @typescript-eslint/no-unused-vars */
import log from "@ajar/marker";
import fs from 'fs';


const { White, Reset, Red } = log.constants;
const { NODE_ENV } = process.env;
import { Request, Response, NextFunction } from "express";


export const error_handler =  (err:Error, req:Request, res:Response, next:NextFunction) => {
    log.error(err);
    next(err);
};

export const logError = ()=>{
    const errorsFileLogger = fs.createWriteStream('./src/server/log/error.log', { flags: 'a' });
    return (err:Error, req: Request, res: Response, next: NextFunction) => {
        errorsFileLogger.write(`${req.id} : ${err.message} >> ${err.stack} \n`);
        next(err);
    }
}
export const error_handler2 =  (err:Error, req:Request, res:Response, next:NextFunction) => {
    if(NODE_ENV !== "production")res.status(500).json({status:err.message,stack:err.stack});
    else res.status(500).json({status:"internal server error..."});
};

export const not_found =  (req:Request, res:Response) => {
    log.info(`url: ${White}${req.url}${Reset}${Red} not found...`);
    res.status(404).json({status:`url: ${req.url} not found...`});
};




