// require('dotenv').config();
import express from "express";
import { genId } from "./middleware/utils.js";
import morgan from "morgan";
// import log from "marker";
import cors from "cors";

import { connect_db } from "./db/mongoose.connection.js";
import user_router from "./modules/user/user.router.js";

import {
    error_handler,
    error_handler2,
    logError,
    not_found,
} from "./middleware/errors.handler.js";
import { user_log } from "./middleware/user-func.js";

const {
    PORT = 8080,
    HOST = "localhost",
    DB_URI = "mongodb://localhost:27017/crud-demo",
} = process.env;
class Api {
  
    private app: express.Application;

    constructor() {
        this.app = express();

        this.applyGlobalMiddleware();
        this.routing();
    }
    applyGlobalMiddleware() {
        this.app.use(cors());
        this.app.use(user_log);
        this.app.use(morgan("dev"));
    }
    routing() {
        // app.use('/api/stories', story_router);
        this.app.use(genId);
        this.app.use("/api/users", user_router);

        // central error handling
        this.app.use(error_handler);
        this.app.use(logError());
        this.app.use(error_handler2);
        //when no routes were matched...
        this.app.use("*", not_found);
    }
    //start the express api server
    async startServer() {
        try {
            //connect to mongo db
            await connect_db(String(DB_URI));
            this.app.listen(Number(PORT), HOST);
            console.log(
                "api is live on",
                ` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`
            );
        } catch (err) {
            console.log;
        }
    }
}

const api = new Api();
api.startServer();
