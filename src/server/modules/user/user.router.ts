/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
  import raw from "../../middleware/route.async.wrapper.js";
  import user_model from "./user.model.js";
  import express from 'express';
  import { Request ,Response } from "express";
  //import log from '@ajar/marker';
  import {checkUser} from "../../validation/validator.js"
import { createUser, deleteById, getAllUsers, getUserByid, updateUser } from "../../service/users._services.js";
  const router = express.Router();
  
  // parse json req.body on post routes
  router.use(express.json())
  
  // CREATES A NEW USER
  router.post("/", checkUser,raw( async (req:Request, res:Response) => {
    //console.log(validate(req.body));
    const user = await createUser(req.body);
    res.status(200).json(user);
  }) );
  
  
  // GET ALL USERS
  router.get( "/",raw(async (req:Request, res:Response) => {
    const users = await getAllUsers();
      res.status(200).json(users);
    })  
  );
  
  
  // GETS A SINGLE USER
  router.get("/:id",raw(async (req:Request, res:Response) => {
      const user = await getUserByid(req.params.id);
      if (!user) return res.status(404).json({ status: "No user found." });
      res.status(200).json(user);
    })
  );
  ////CHECK
  router.get("/pagination/:page/:count",raw(async (req:Request, res:Response) => {
    const users = await user_model.find().skip(Number(req.params.page)*Number(req.params.count)).limit(Number(req.params.count));                
    res.status(200).json(users);
  })
  );
  // UPDATES A SINGLE USER
  router.put("/:id",checkUser,raw(async (req:Request, res:Response) => {
      const user = await updateUser(req.params.id,req.body);
      res.status(200).json(user);
    })
  );
  

  // DELETES A USER
  router.delete("/:id",raw(async (req:Request, res:Response) => {
      const user = await deleteById(req.params.id);
      if (!user) return res.status(404).json({ status: "No user found." });
      res.status(200).json(user);
    })
  );
  
  export default router;
  