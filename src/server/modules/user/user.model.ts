import mongoose from "mongoose";
const { Schema, model } = mongoose;

const UserSchema = new Schema({
    first_name  : {type : String, require: [true,"you must add first_name"]},
    last_name   : {type: String,require: [true,"you must add last_name"]},
    email       : {type: String, require:true},
    phone       : {type: String,
        validate: {
            validator: function(v:string){
                return /[0-9]{8,12}/.test(v);
            },
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            message: (props:any) => `${props.value} isnt a valid phone number`
            },
        require: [true,"you must add phone"]
     }
}, {timestamps:true});
  
export default model("user",UserSchema);
