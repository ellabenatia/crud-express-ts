/* eslint-disable @typescript-eslint/no-explicit-any */
  import user_model from "../modules/user/user.model.js";

  export async function createUser(user:any){
    return user_model.create(user);
   }

   export async function getAllUsers(){
    return user_model.find()
    // .select(`-__v`);
    .select(`-_id 
            first_name 
            last_name 
            email 
            phone`);
   }
export function getUserByid(id:string) {
    return user_model.findById(id);
    // .select(`-_id 
    //     first_name 
    //     last_name 
    //     email
    //     phone`);
}
export function updateUser(id:string, user:any) {
    return user_model.findByIdAndUpdate(id,user, 
        {new: true, upsert: false });
}

export function deleteById(id:string){
    return user_model.findByIdAndRemove(id);
}


